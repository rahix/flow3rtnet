import network
import machine
import time
import leds

from .stupidArtnet import StupidArtnetServer

from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from ctx import Context
import st3m.run

class Flow3rtNet(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        # Ignore the app_ctx for now.
        print("Initializing device")
        wlan = network.WLAN(network.STA_IF)
        print("Activating device")
        wlan.active(True)
        ssid = "flow3rtnet"
        print(f"Connecting to {ssid}")
        wlan.connect(ssid, "artnet11")
        while not wlan.isconnected():
            print("Not yet connected")
            #machine.lightsleep(1000)
            time.sleep_ms(1000)
        print("Connected!")
        while wlan.ifconfig()[0] == '0.0.0.0':
            print("Waiting for DHCP")
            #machine.lightsleep(1000)
            time.sleep_ms(1000)
        print(f"DHCP successful. ifconfig: {wlan.ifconfig()}")

        # create a callback to handle data when received
        def parse_data(data, addr):
            # the received data is an array
            # of the channels value (no headers)
            print('Received new data \n', data)
            leds.set_brightness(data[0])
            
            if data[1] > 0 or data[2] > 0 or data[3] > 0:
                leds.set_all_rgb(data[1]/255, data[2]/255, data[3]/255)
            else:
                leds.set_all_rgb(0,0,0)
                offset = 4

                num_rgb_values = len(data) - offset

                num_leds = min(40, int(num_rgb_values / 3))

                for i in range(num_leds):
                    leds.set_rgb(i, data[offset + i * 3] / 255,
                                    data[offset + i * 3 + 1] / 255,
                                    data[offset + i * 3 + 2] / 255)

                # If there is data for not all 3 channels of the last LED, still use it
                num_remaining_channels = num_rgb_values % 3
                if num_remaining_channels == 1:
                    leds.set_rgb(i, data[offset + i * 3] / 255, 0, 0)
                if num_remaining_channels == 2:
                    leds.set_rgb(i, data[offset + i * 3] / 255,
                                 data[offset + i * 3 + 1] / 255,
                                 0)
            
            leds.update()

        print("Creating StupidArtnetServer")
        a = StupidArtnetServer()
        print("Creating listener")
        u1_listener = a.register_listener(0, callback_function=parse_data)

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        # Red square
        ctx.rgb(255, 0, 0).rectangle(-20, -20, 40, 40).fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(Flow3rtNet(ApplicationContext()))
