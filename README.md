# Flow3rtNet
Artnet integration for [flow3r](https://flow3r.garden), the badge of the Chaos Communication Camp 2023.

## Usage
1. Copy the [flow3r.toml](flow3r.toml) and the [__init__.py](__init__.py) into a folder called `flow3rtnet` (or something else) under `/flash/sys/apps/` on your flow3r. For example, use the "Mount Flash" option from the flow3r system menu to access your flow3r like a USB device.
2. Create a WiFi AP with SSID `flow3rtnet` and PSK `artnet11` (at the moment, the credentials are hardcoded) as Camp WiFi does not allow broadcast traffic. For example using `create_ap`:
    ```bash
    sudo create_ap wlo1 eno1 flow3rtnet artnet11
    ```
3. Launch the `flow3rtnet` app from the app menu of your flow3r
4. (Optional) Connect to the repl of your flow3r using `mpremote repl` to see debug output, e.g. connection status
5. Wait for your flow3r to connect to your WiFi network. For example, `create_ap` log will look like this:
    ```
   Config dir: /tmp/create_ap.wlo1.conf.AecrfSNV
    PID: 107239
    Network Manager found, set ap0 as unmanaged device... DONE
    Creating a virtual WiFi interface... ap0 created.
    Sharing Internet using method: nat
    hostapd command-line interface: hostapd_cli -p /tmp/create_ap.wlo1.conf.AecrfSNV/hostapd_ctrl
    WARN: Low entropy detected. We recommend you to install `haveged'
    ap0: interface state UNINITIALIZED->ENABLED
    ap0: AP-ENABLED 
    ap0: STA 34:85:18:78:0b:c4 IEEE 802.11: authenticated
    ap0: STA 34:85:18:78:0b:c4 IEEE 802.11: associated (aid 1)
    ap0: AP-STA-CONNECTED 34:85:18:78:0b:c4
    ap0: STA 34:85:18:78:0b:c4 RADIUS: starting accounting session 736E23F53A8FD74C
    ap0: STA 34:85:18:78:0b:c4 WPA: pairwise key handshake completed (RSN)
    ap0: EAPOL-4WAY-HS-COMPLETED 34:85:18:78:0b:c4
   ```
6. Send artnet packages to the IP address of your flow3r, or the broadcast address of your WiFi network, e.g. by using the [stupidArtnet](https://pypi.org/project/stupidArtnet/) Python library:
    ```
    >>> from stupidArtnet import StupidArtnet
    >>> a = StupidArtnet('192.168.12.255', 0, 255, 30, True, True)
    >>> a.flash_all()
    ```
   Now all your LEDs should light up
7. ???
8. Profit!

## Channel layout
Currently, the flow3r listens on ArtNet universe 1 channel 0 hardcoded. The channels are as follows:

| Channel No. | Feature                                          |
|-------------|--------------------------------------------------|
| 0           | Global Dimmer (0=off, 255=max brightness)        |
| 1 - 3       | RGB values for all LEDs, ignored if all are 0    |
| 4 - 6       | RGB values for LED 0 (0=off, 255=max brightness) |
| 7 - 9       | RGB values for LED 1                             |
| ...         | ...                                              |
| 121 - 123   | RGB values for LED 39                            |

## Usage with QLC+
[QLC+](https://www.qlcplus.org/) is a popular open source light mixing software with built-in ArtNet support. It is possible to control the flow3r from QLC+ along with other (DMX, ArtNet, ...) lights.

![Example video showing the flow3r badge being controlled by QLC+](.doc/static/QLCExample.mp4)

1. Follow steps 1-5 from usage
2. Open QLC+ on a computer connected to the `flow3rtnet` WiFi network (machine running `create_ap` works as well)

![QLC+ showing the Input/Output configuration screen](.doc/static/QLCInputOutput.png)

1. Select "Inputs/Outputs" at the bottom of the QLC+ window
2. Make sure the first universe is selected
3. Tick the "output" box next to the ArtNet plugin listing the IP address of the wireless network your flow3r is in
4. Select the tool icon (4) to change settings for this ArtNet plugin.

![QLC+ showing the ArtNet plugin configuration screen](.doc/static/QLCArtNetConfiguration.png)

1. In the "Universes Configuration" tab (selected by default), make sure the correct IP address is listed under "Outputs".
2. In that row, Universe should be 1, as you selected the first universe in step 2 of the previous screen.
3. IP Address should show either the IP address of your flower, or the broadcast IP address of that network (`.255` for a `/24` network)
4. ArtNet Universe must be 0, as this is currently hardcoded in `flow3rtnet`
5. Transmission mode should be set to partial to increase performance and reduce WiFi traffic.

![QLC+ showing the basic mixing interface](.doc/static/QLCSimpleDesk.png)

1. Open the basic mixing interface by clicking "Simple Desk" at the bottom of the screen
2. Make sure the "Grand Master" fader is set to 100%
3. Use the other faders to control the channels as described in the channel table above, e.g. set the first 5 faders to 100% to light up all LEDs in white.

Enjoy!

## Upcoming
Ideas for the future:
- [ ] Make ArtNet universe id customizable 
- [ ] Make WiFi credentials customizable
- [ ] Status output on the flow3r display
- [ ] Use display as one big RGB LED
  - How to trigger display updates from thread?
- [ ] Add menu to enter WiFi credentials, ArtNet universe id or channel.
- [ ] Custom user defined channel mapping
- [ ] Animations
- [ ] Color wheel
- [ ] User customizable animations
- [ ] Strobe channel